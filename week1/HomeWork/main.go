package main

import (
	"fmt"
	"io"
	"os"
	"path/filepath"
	"strconv"
	"strings"
)

func findLastDirs(dirs []string) (lastDirs map[string]string, lastMain string) {
	lastDirs = make(map[string]string)
	for dir := 0; dir < len(dirs); dir++ {
		s := strings.Split(dirs[dir], string(os.PathSeparator))
		for sCnt := 1; sCnt < len(s); sCnt++ {
			if dir+1 < len(dirs) {
				nextS := strings.Split(dirs[dir+1], string(os.PathSeparator))
				if len(s) != len(nextS) && s[1] == nextS[1] {
					lastDirs[s[sCnt-1]] = s[sCnt]
				}

			} else {
				lastDirs[s[0]] = s[1]
				lastMain = s[1]
			}
		}
	}
	//fmt.Printf("lastDirs: %#v \n", lastDirs)
	//fmt.Printf("lastMain: %#v \n", lastMain)
	return
}

func drawTree(outs []string, sizes map[string]string) string {
	var output string
	lastDirs, lastMain := findLastDirs(outs)

	for out := 0; out < len(outs); out++ {
		if strings.Contains(outs[out], ".DS_Store") {
			continue
		}

		s := strings.Split(outs[out], string(os.PathSeparator))
		for sCnt := 1; sCnt < len(s); sCnt++ {
			if sCnt < len(s) - 1 {

				if out + 1 < len(outs) {
					nextS := strings.Split(outs[out+1], string(os.PathSeparator))
					if len(s) == len(nextS) && s[sCnt] != nextS[sCnt] {
						output += "\t"
					} else {
						if lastDirs[s[1]] == s[sCnt] {
							output += "\t"
						} else if s[1] == lastMain {
							output += "\t"
						} else if strings.Contains(s[sCnt+1], ".") && lastDirs[s[sCnt]] == s[sCnt+1] && lastDirs[s[sCnt-1]] == s[sCnt] {
							output += "\t"
						} else {
							output += "│\t"
						}

					}
				} else {
					output += "\t"
				}
			} else {
				if out + 1 < len(outs) {
					nextS := strings.Split(outs[out+1], string(os.PathSeparator))
					if len(s) == len(nextS) && s[sCnt] != nextS[sCnt] {
						output += "├───" + s[sCnt]
					} else {
						//fmt.Printf("LastDirs: %#v \n", lastDirs)
						isLast := lastDirs[s[sCnt-1]] == s[sCnt]
						//fmt.Printf("%v is last dir? - %v \n", s[sCnt], isLast)
						if sCnt < len(nextS) && s[sCnt] == nextS[sCnt] && !isLast && s[sCnt] != lastMain {
							output += "├───" + s[sCnt]
						} else {
							//println(s[sCnt])
							output += "└───" + s[sCnt]
						}
					}
				} else {
					output += "└───" + s[sCnt]
				}

				if strings.Contains(s[sCnt], ".") {
					output += " (" + sizes[outs[out]] + ")"
				}

			}
		}

		if out < len(outs) - 1 {
			output += "\n"
		}

	}

	return output
}

func dirTree(writer io.Writer, path string, printFiles bool) error {
	var outs []string
	var sizes = make(map[string]string)
	dirToOut := "." + string(os.PathSeparator) + path
	err := filepath.Walk(dirToOut, func(path string, info os.FileInfo, err error) error {
		if dirToOut != path {
			if printFiles {
				outs = append(outs, path)
				if info.Size() == 0 {
					sizes[path] = "empty"
				} else {
					sizes[path] = strconv.FormatInt(info.Size(), 10) + "b"
				}
			} else {
				if !strings.Contains(path, ".") {
					outs = append(outs, path)
				}
			}
		}
		return nil
	})

	txt := drawTree(outs, sizes)
	fmt.Fprintln(writer, txt)

	return err
}

//func dirTree(writer io.Writer, path string, printFiles bool) error {
//		var outs []string
//		var sizes = make(map[string]string)
//		dirToOut := "." + string(os.PathSeparator) + path
//
//		err := filepath.Walk(dirToOut, func(path string, info os.FileInfo, err error) error {
//			if dirToOut != path {
//				if printFiles {
//					outs = append(outs, path)
//					if info.Size() == 0 {
//						sizes[path] = "empty"
//					} else {
//						sizes[path] = strconv.FormatInt(info.Size(), 10) + "b"
//					}
//				} else {
//					if !strings.Contains(path, ".") {
//						outs = append(outs, path)
//					}
//				}
//			}
//			return nil
//		})
//
//		//fmt.Printf("%#v \n", sizes)
//		fmt.Printf("%#v \n", outs)
//
//		txt := "test"
//
//		fmt.Fprintln(writer, txt)
//
//		return err
//}

func main() {
	out := os.Stdout
	if !(len(os.Args) == 2 || len(os.Args) == 3) {
		panic("usage go run main.go . [-f]")
	}
	path := os.Args[1]
	printFiles := len(os.Args) == 3 && os.Args[2] == "-f"
	err := dirTree(out, path, printFiles)
	if err != nil {
		panic(err.Error())
	}
}
